package test;

import main.programs.DayFourPartTwo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DayFourPartTwoTest {

  DayFourPartTwo underTest;

  @Before
  public void init(){
    underTest = new DayFourPartTwo();
  }

  @Test
  public void test0(){
    //given
    int i = 112233;
    //when
    boolean actual = underTest.isPasswordValid(i);
    //then
    Assert.assertTrue(actual);
  }

  @Test
  public void test1(){
    //given
    int i = 123444;
    //when
    boolean actual = underTest.isPasswordValid(i);
    //then
    Assert.assertFalse(actual);
  }

  @Test
  public void test2(){
    //given
    int i = 111122;
    //when
    boolean actual = underTest.isPasswordValid(i);
    //then
    Assert.assertTrue(actual);
  }

  @Test
  public void test3(){
    //given
    int i = 123456;
    //when
    boolean actual = underTest.isPasswordValid(i);
    //then
    Assert.assertFalse(actual);
  }

  @Test
  public void test4(){
    //given
    int i = 111111;
    //when
    boolean actual = underTest.isPasswordValid(i);
    //then
    Assert.assertFalse(actual);
  }

  @Test
  public void test5(){
    //given
    int i = 111667;
    //when
    boolean actual = underTest.isPasswordValid(i);
    //then
    Assert.assertTrue(actual);
  }

}
