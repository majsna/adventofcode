package test;

import main.programs.DayTwo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DayTwoTest {

  private DayTwo underTest;

  @Before
  public void init() {
    underTest = new DayTwo();
  }

  @Test
  public void testDayTwoIncodeProgram() {
    //given
    String input = "1,9,10,3,2,3,11,0,99,30,40,50";
    String expectedMemory = "3500,9,10,70,2,3,11,0,99,30,40,50";
    String expectedFirstPositionNumber = "3500";
    //when
    String actualFirstPositionNumber = underTest.calculateIntcode(input);
    String actualMemory = underTest.getResultMemory();
    //then
    Assert.assertEquals("Should match", expectedFirstPositionNumber, actualFirstPositionNumber);
    Assert.assertEquals("Should match", expectedMemory, actualMemory);
  }

  @Test
  public void testDayTwoIncodeProgram1() {
    //given
    String input = "1,0,0,0,99";
    String expectedMemory = "2,0,0,0,99";
    String expectedFirstPositionNumber = "2";
    //when
    String actualFirstPositionNumber = underTest.calculateIntcode(input);
    String actualMemory = underTest.getResultMemory();
    //then
    Assert.assertEquals("Should match", expectedFirstPositionNumber, actualFirstPositionNumber);
    Assert.assertEquals("Should match", expectedMemory, actualMemory);
  }

  @Test
  public void testDayTwoIncodeProgram2() {
    //given
    String input = "2,3,0,3,99";
    String expectedMemory = "2,3,0,6,99";
    String expectedFirstPositionNumber = "2";
    //when
    String actualFirstPositionNumber = underTest.calculateIntcode(input);
    String actualMemory = underTest.getResultMemory();
    //then
    Assert.assertEquals("Should match", expectedFirstPositionNumber, actualFirstPositionNumber);
    Assert.assertEquals("Should match", expectedMemory, actualMemory);
  }

  @Test
  public void testDayTwoIncodeProgram3() {
    //given
    String input = "2,4,4,5,99,0";
    String expectedMemory = "2,4,4,5,99,9801";
    String expectedFirstPositionNumber = "2";
    //when
    String actualFirstPositionNumber = underTest.calculateIntcode(input);
    String actualMemory = underTest.getResultMemory();
    //then
    Assert.assertEquals("Should match", expectedFirstPositionNumber, actualFirstPositionNumber);
    Assert.assertEquals("Should match", expectedMemory, actualMemory);
  }

  @Test
  public void testDayTwoIncodeProgram4() {
    //given
    String input = "1,1,1,4,99,5,6,0,99";
    String expectedMemory = "30,1,1,4,2,5,6,0,99";
    String expectedFirstPositionNumber = "30";
    //when
    String actualFirstPositionNumber = underTest.calculateIntcode(input);
    String actualMemory = underTest.getResultMemory();
    //then
    Assert.assertEquals("Should match", expectedFirstPositionNumber, actualFirstPositionNumber);
    Assert.assertEquals("Should match", expectedMemory, actualMemory);
  }

}
