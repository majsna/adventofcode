package test;

import java.util.Arrays;
import java.util.List;
import main.programs.DayOne;
import main.utils.StringToListMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DayOneTest {

  public static final String FUEL_INPUT = "137113\n91288\n62216\n61150\n143536\n69244\n102261\n105683\n58305\n67377\n107379\n108666\n56279\n123299\n120794\n60286\n112665\n144945\n100039\n60631\n77509\n106891\n103638\n132144\n119960\n96479\n131631\n105498\n124620\n88703\n101268\n72720\n135531\n108871\n90019\n129257\n69947\n69968\n104725\n95262\n119107\n111562\n81709\n102441\n129733\n84750\n101748\n107232\n113844\n115357\n125062\n83869\n69129\n79132\n144282\n115941\n144188\n58559\n92455\n135538\n146503\n142974\n73517\n112043\n143187\n130617\n144656\n114329\n130205\n92973\n134265\n120776\n62569\n145143\n131663\n130428\n121409\n109042\n111748\n99222\n102198\n63934\n130811\n139884\n107805\n107306\n140757\n149374\n119437\n131554\n55182\n69234\n54593\n92531\n69679\n111405\n143524\n66057\n93150\n53854";

  private DayOne underTest;

  @Before
  public void init(){
    underTest = new DayOne();
  }

  @Test
  public void testCalculator(){
    //given
    List<Integer> givenMassList = Arrays.asList(14, 1969);
    int expectedFuel = 968;
    //when
    int actualFuel = underTest.calculateFuel(givenMassList);
    //then
    Assert.assertEquals("Actual fule amount does not match the expected one.",expectedFuel, actualFuel);
  }

  @Test
  public void testCalculatorForDayTwoInput(){
    //given
    List<Integer> givenMassList = StringToListMapper.map(FUEL_INPUT, "\n");
    int expectedFuel = 5233250;
    //when
    int actualFuel = underTest.calculateFuel(givenMassList);
    //then
    Assert.assertEquals("Actual fuel amount does not match the expected one.",expectedFuel, actualFuel);
  }


}
