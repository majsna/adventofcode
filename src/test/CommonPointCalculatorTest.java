package test;

import main.programs.daythree.CommonPointCalc;
import main.programs.daythree.Point;
import main.programs.daythree.Section;
import org.junit.Assert;
import org.junit.Test;

public class CommonPointCalculatorTest {


  @Test
  public void test0() {
    //given
    Section one = new Section();
    one.a = new Point(3, 6);
    one.b = new Point(3, 2);
    Section two = new Section();
    two.a = new Point(2, 3);
    two.b = new Point(6, 3);
    Point expectedCommonPoint = new Point(3, 3);
    //when
    Point actualCommonPoint = CommonPointCalc.findCommonPoint(one, two);
    //then
    Assert.assertEquals(expectedCommonPoint, actualCommonPoint);
  }

  @Test
  public void test1() {
    //given
    Section one = new Section();
    one.a = new Point(3, 6);
    one.b = new Point(3, 2);
    Section two = new Section();
    two.a = new Point(6, 3);
    two.b = new Point(2, 3);
    Point expectedCommonPoint = new Point(3, 3);
    //when
    Point actualCommonPoint = CommonPointCalc.findCommonPoint(one, two);
    //then
    Assert.assertEquals(expectedCommonPoint, actualCommonPoint);
  }

  @Test
  public void test2() {
    //given
    Section one = new Section();
    one.a = new Point(1, 2);
    one.b = new Point(4, 2);
    Section two = new Section();
    two.a = new Point(3, 5);
    two.b = new Point(3, 1);
    Point expectedCommonPoint = new Point(3,2);
    //when
    Point actualCommonPoint = CommonPointCalc.findCommonPoint(one, two);
    //then
    Assert.assertEquals(expectedCommonPoint, actualCommonPoint);
  }

  @Test
  public void test3() {
    //given
    Section one = new Section();
    one.a = new Point(0, 0);
    one.b = new Point(0, 6);
    Section two = new Section();
    two.a = new Point(0, 0);
    two.b = new Point(3, 0);
    Point expectedCommonPoint = null;
    //when
    Point actualCommonPoint = CommonPointCalc.findCommonPoint(one, two);
    //then
    Assert.assertEquals(expectedCommonPoint, actualCommonPoint);
  }

  @Test
  public void test4() {
    //given
    Section one = new Section();
    one.a = new Point(2, 2);
    one.b = new Point(6, 2);
    Section two = new Section();
    two.a = new Point(3, 3);
    two.b = new Point(3, 7);
    Point expectedCommonPoint = null;
    //when
    Point actualCommonPoint = CommonPointCalc.findCommonPoint(one, two);
    //then
    Assert.assertEquals(expectedCommonPoint, actualCommonPoint);
  }

}
