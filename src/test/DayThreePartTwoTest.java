package test;

import main.programs.daythree.DayThreePartTwo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DayThreePartTwoTest {

  DayThreePartTwo underTest;

  @Before
  public void init() {
    underTest = new DayThreePartTwo();
  }

  @Test
  public void test0() {
    //given
    String input = "R8,U5,L5,D3S"
        + "U7,R6,D4,L4";
    String expected = "30.0";
    //when
    String actual = underTest.calculate(input);
    //then
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void test1() {
    //given
    String input = "R75,D30,R83,U83,L12,D49,R71,U7,L72S"
        + "U62,R66,U55,R34,D71,R55,D58,R83";
    String expected = "610.0";
    //when
    String actual = underTest.calculate(input);
    //then
    Assert.assertEquals(expected, actual);
  }

@Test
  public void test2() {
    //given
    String input = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51S"
        + "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
    String expected = "410.0";
    //when
    String actual = underTest.calculate(input);
    //then
    Assert.assertEquals(expected, actual);
  }

}
