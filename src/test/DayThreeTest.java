package test;

import static main.programs.daythree.Direction.D;
import static main.programs.daythree.Direction.L;
import static main.programs.daythree.Direction.R;
import static main.programs.daythree.Direction.U;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import main.programs.daythree.DayThree;
import main.programs.daythree.Element;
import main.programs.daythree.Point;
import main.programs.daythree.Section;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DayThreeTest {

  DayThree underTest;

  @Before
  public void init() {
    this.underTest = new DayThree();
  }

  @Test
  public void shouldCalculatePointBCase1() {
    //given
    Point a = new Point(0, 0);
    Element el = new Element(R, 5);
    Point expectedB = new Point(5, 0);

    //then
    shouldCalculatePointBWithGiven(a, expectedB, el);
  }

  @Test
  public void shouldCalculatePointBCase2() {
    //given
    Point a = new Point(0, 0);
    Element el = new Element(L, 7);
    Point expectedB = new Point(-7, 0);

    //then
    shouldCalculatePointBWithGiven(a, expectedB, el);
  }

  @Test
  public void shouldCalculatePointBCase3() {
    //given
    Point a = new Point(0, 0);
    Element el = new Element(U, 4);
    Point expectedB = new Point(0, 4);

    //then
    shouldCalculatePointBWithGiven(a, expectedB, el);
  }

  @Test
  public void shouldCalculatePointBCase4() {
    //given
    Point a = new Point(0, 0);
    Element el = new Element(D, 15);
    Point expectedB = new Point(0, -15);

    //then
    shouldCalculatePointBWithGiven(a, expectedB, el);
  }

  @Test
  public void shouldGetSectionsFromWireCase1() {
    //given
    List<Element> givenWire = new ArrayList<>();
    givenWire.add(new Element(R, 5));
    givenWire.add(new Element(U, 7));
    List<Section> expectedSection = new ArrayList<>();
    expectedSection.add(new Section(new Point(0, 0), new Point(5, 0)));
    expectedSection.add(new Section(new Point(5, 0), new Point(5, 7)));

    //when
    List<Section> actualSections = underTest.getSectionsFrom(givenWire);

    //then
    Assert.assertTrue(Objects.equals(expectedSection, actualSections));
  }

  @Test
  public void shouldGetSectionsFromWireCase2() {
    //given
    List<Element> givenWire = new ArrayList<>();
    givenWire.add(new Element(R, 8));
    givenWire.add(new Element(U, 5));
    givenWire.add(new Element(L, 5));
    givenWire.add(new Element(D, 3));
    List<Section> expectedSection = new ArrayList<>();
    expectedSection.add(new Section(new Point(0, 0), new Point(8, 0)));
    expectedSection.add(new Section(new Point(8, 0), new Point(8, 5)));
    expectedSection.add(new Section(new Point(8, 5), new Point(3, 5)));
    expectedSection.add(new Section(new Point(3, 5), new Point(3, 2)));

    //when
    List<Section> actualSections = underTest.getSectionsFrom(givenWire);

    //then
    Assert.assertTrue(Objects.equals(expectedSection, actualSections));
  }

  @Test
  public void shouldFindTheClosestIntersectionCase1(){
    //given
    List<Section> givenFirstWierSections = new ArrayList<>();
    givenFirstWierSections.add(new Section( new Point(3, 2),new Point(3, 5)));
    List<Section> givenSecontWireSections = new ArrayList<>();
    givenSecontWireSections.add(new Section( new Point(2, 3),new Point(6, 3)));
    int expectedClosestIntersectionDisance = 6;
    //when
    int actualClosestIntersectionDisance = underTest.findClosesIntersectionDistance(givenFirstWierSections, givenSecontWireSections);

    //then
    Assert.assertEquals(expectedClosestIntersectionDisance, actualClosestIntersectionDisance);
  }

  @Test
  public void shouldFindTheClosestIntersectionCase2(){
    //given
    List<Section> givenFirstWierSections = new ArrayList<>();
    givenFirstWierSections.add(new Section(new Point(0, 0), new Point(8, 0)));
    givenFirstWierSections.add(new Section(new Point(8, 0), new Point(8, 5)));
    givenFirstWierSections.add(new Section(new Point(8, 5), new Point(3, 5)));
    givenFirstWierSections.add(new Section(new Point(3, 5), new Point(3, 2)));
    List<Section> givenSecontWireSections = new ArrayList<>();
    givenSecontWireSections.add(new Section(new Point(0, 0), new Point(0, 7)));
    givenSecontWireSections.add(new Section(new Point(0, 7), new Point(6, 7)));
    givenSecontWireSections.add(new Section(new Point(6, 7), new Point(6, 3)));
    givenSecontWireSections.add(new Section(new Point(6, 3), new Point(2, 3)));
    int expectedClosestIntersectionDisance = 6;
    //when
    int actualClosestIntersectionDisance = underTest.findClosesIntersectionDistance(givenFirstWierSections, givenSecontWireSections);

    //then
    Assert.assertEquals(expectedClosestIntersectionDisance, actualClosestIntersectionDisance);
  }

  @Test
  public void shouldFindTheClosestIntersectionCase3(){
    //given
    List<Section> givenFirstWierSections = new ArrayList<>();
    givenFirstWierSections.add(new Section(new Point(1, 2), new Point(4, 2)));
    List<Section> givenSecontWireSections = new ArrayList<>();
    givenSecontWireSections.add(new Section( new Point(3, 5),new Point(3, 1)));
    int expectedClosestIntersectionDisance = 5;
    //when
    int actualClosestIntersectionDisance = underTest.findClosesIntersectionDistance(givenFirstWierSections, givenSecontWireSections);

    //then
    Assert.assertEquals(expectedClosestIntersectionDisance, actualClosestIntersectionDisance);
  }


  @Test
  public void shouldParseInput(){
    //given
    String input = "R8,U5,L5,D3SU7,R6,D4,L4";
    List<Element> expectedFirsWire = new ArrayList<>();
    expectedFirsWire.add(new Element(R,8));
    expectedFirsWire.add(new Element(U,5));
    expectedFirsWire.add(new Element(L,5));
    expectedFirsWire.add(new Element(D,3));
    List<Element> expectedSecondWire = new ArrayList<>();
    expectedSecondWire.add(new Element(U,7));
    expectedSecondWire.add(new Element(R,6));
    expectedSecondWire.add(new Element(D,4));
    expectedSecondWire.add(new Element(L,4));
    //when
    underTest.parseInput(input);
    //then
    Assert.assertEquals(expectedFirsWire, underTest.firstWire);
    Assert.assertEquals(expectedSecondWire, underTest.secondWire);
  }

  @Test
  public void shouldCalculateCase0(){
    //given
    String input = "R8,U5,L5,D3S"
        + "U7,R6,D4,L4";
    String expected = "6";
    //when
    String actual = underTest.calculate(input);
    //then
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void shouldCalculateCase1(){
    //given
    String input = "R75,D30,R83,U83,L12,D49,R71,U7,L72S"
        + "U62,R66,U55,R34,D71,R55,D58,R83";
    String expected = "159";
    //when
    String actual = underTest.calculate(input);
    //then
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void shouldCalculateCase2(){
    //given
    String input = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51S"
        + "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
    String expected = "135";
    //when
    String actual = underTest.calculate(input);
    //then
    Assert.assertEquals(expected, actual);
  }

  private void shouldCalculatePointBWithGiven(Point givenA, Point expectedB, Element givenEl) {
    //when
    Point actualB = underTest.calculatePointB(givenA, givenEl);
    //then
    Assert.assertEquals(expectedB, actualB);
  }
}
