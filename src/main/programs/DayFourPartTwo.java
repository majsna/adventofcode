package main.programs;

public class DayFourPartTwo extends DayFour {

  @Override
  public boolean isPasswordValid(int i) {
    char[] iChars = (i + "").toCharArray();
    int[] frequences = new int[7];
    int sameDigitsCounter = 1;
    boolean digitsIncrease = true;
    for (int j = 1; j < iChars.length; j++) {
      if (digitsDecrease(iChars, j)) {
        digitsIncrease = false;
        break;
      } else if (sameDigits(iChars, j)) {
        sameDigitsCounter++;
      } else {
        frequences[j] = sameDigitsCounter;
        sameDigitsCounter = 1;
      }
    }
    if (!digitsIncrease) {
      return false;
    }
    frequences[6] = sameDigitsCounter;
    return hasAtLeastOneDouble(frequences);
  }

  private boolean hasAtLeastOneDouble(int[] frequences) {
    for (int f : frequences) {
      if (f == 2) {
        return true;
      }
    }
    return false;
  }
}
