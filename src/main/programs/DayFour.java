package main.programs;

public class DayFour implements Calculator {

  @Override
  public String calculate(String input) {
    String[] range = input.split("-");
    int min = Integer.parseInt(range[0]);
    int max = Integer.parseInt(range[1]);
    int counter = 0;
    for (int i = min; i <= max; i++) {
      if(isPasswordValid(i)){
        counter++;
      }
    }
    return counter + "";
  }

  public boolean isPasswordValid(int i){
    char[] iChars = (i + "").toCharArray();
    int sameDigitsCounter = 0;
    boolean digitsIncrease = true;
    for (int j = 1; j < iChars.length; j++) {
      if (digitsDecrease(iChars, j)) {
        digitsIncrease = false;
        break;
      }
      if (sameDigits(iChars, j)) {
        sameDigitsCounter++;
      }
    }
    return digitsIncrease && sameDigitsCounter > 0;
  }

  protected boolean sameDigits(char[] iChars, int j) {
    return Integer.parseInt(String.valueOf(iChars[j])) ==
        Integer.parseInt(String.valueOf(iChars[j - 1]));
  }

  protected boolean digitsDecrease(char[] iChars, int j) {
    return Integer.parseInt(String.valueOf(iChars[j])) <
        Integer.parseInt(String.valueOf(iChars[j - 1]));
  }
}
