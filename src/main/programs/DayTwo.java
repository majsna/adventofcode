package main.programs;

import java.util.List;
import main.utils.ListToStringMapper;
import main.utils.StringToListMapper;

public class DayTwo {

  protected final int STOP_PROGRAM_OPTCODE = 99;
  protected final int ADD_NUMBERS_OPTCODE = 1;
  protected final int MULTIPLY_NUMBERS_OPTCODE = 2;

  protected List<Integer> memory;

  protected int instruction;
  protected int firstParamPosition;
  protected int secondParamPosition;
  protected int resultPosition;
  protected int resultValue;
  protected String resultMemory;

  public String calculateIntcode(String input) {
    memory = StringToListMapper.map(input, ",");
    try {
      int countTo4 = 0;
      for (Integer i : memory) {
        if (countTo4 == 4) {
          countTo4 = 0;
        }
        switch (countTo4) {
          case 0:
            instruction = i;
            break;
          case 1:
            firstParamPosition = i;
            break;
          case 2:
            secondParamPosition = i;
            break;
          case 3:
            resultPosition = i;
            calculateResultAndMoveToPosition();
            break;
          default:
            throw new IllegalArgumentException("Wrong counter value.");
        }
        countTo4++;
      }
    } catch (RuntimeException e) {
    }
    resultMemory = ListToStringMapper.map(memory);
    return memory.get(0).toString();
  }

  private void calculateResultAndMoveToPosition() {
    if (instruction == ADD_NUMBERS_OPTCODE) {
      resultValue = memory.get(firstParamPosition) + memory.get(secondParamPosition);
    } else if (instruction == MULTIPLY_NUMBERS_OPTCODE) {
      resultValue = memory.get(firstParamPosition) * memory.get(secondParamPosition);
    } else if (instruction == STOP_PROGRAM_OPTCODE) {
      throw new RuntimeException("STOP");
    }
    memory.set(resultPosition, resultValue);
  }

  public String getResultMemory(){
    return this.resultMemory;
  }

}
