package main.programs;

public interface Calculator {

  String calculate(String input);

}
