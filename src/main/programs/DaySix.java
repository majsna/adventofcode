package main.programs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;

public class DaySix implements Calculator {


  @Override
  public String calculate(String input) {
    int counter = 0;
    List<Orbit> orbits = mapToOrbits(input);
    Map<String, Integer> planetOrbitsMap = new HashMap<>();
    orbits.stream().forEach(o -> {
      planetOrbitsMap.put(o.A, 0);
      planetOrbitsMap.put(o.inOrbitOfA, 0);
    });
    for (String planet : planetOrbitsMap.keySet()) {
      counter = counter + getOrbitsCount(planet, orbits);
    }
    return counter + "";
  }

  private int getOrbitsCount(String planet, List<Orbit> orbits) {
    int counter = 0;
    boolean hasDirectOrbit = false;
    loop1:
    do {
      for (Orbit o : orbits) {
        if (o.inOrbitOfA.equals(planet)) {
          counter++;
          hasDirectOrbit = true;
          planet = o.A;
          continue loop1;
        }
      }
      hasDirectOrbit = false;
    } while (hasDirectOrbit);
    return counter;
  }

  private List<Orbit> mapToOrbits(String input) {
    return
        new LinkedList<>(Arrays.asList(input.split("\n"))).stream()
            .map(o -> new Orbit(o))
            .collect(Collectors.toList());
  }


  class Orbit {

    String A;
    String inOrbitOfA;

    public Orbit(String o) {
      String[] s = o.split("\\)");
      this.A = s[0];
      this.inOrbitOfA = s[1];
    }

    @Override
    public String toString() {
      return "Orbit{" +
          "A='" + A + '\'' +
          ", inOrbitOfA='" + inOrbitOfA + '\'' +
          '}';
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Orbit orbit = (Orbit) o;
      return A.equals(orbit.A) &&
          inOrbitOfA.equals(orbit.inOrbitOfA);
    }

    @Override
    public int hashCode() {
      return Objects.hash(A, inOrbitOfA);
    }
  }

  @Test
  public void testCase0() {
    //given
    String input =
        "COM)B\n"
            + "B)C\n"
            + "C)D\n"
            + "D)E\n"
            + "E)F\n"
            + "B)G\n"
            + "G)H\n"
            + "D)I\n"
            + "E)J\n"
            + "J)K\n"
            + "K)L";
    List<Orbit> expected = new LinkedList<>();
    expected.add(new Orbit("COM)B"));
    expected.add(new Orbit("B)C"));
    expected.add(new Orbit("C)D"));
    expected.add(new Orbit("D)E"));
    expected.add(new Orbit("E)F"));
    expected.add(new Orbit("B)G"));
    expected.add(new Orbit("G)H"));
    expected.add(new Orbit("D)I"));
    expected.add(new Orbit("E)J"));
    expected.add(new Orbit("J)K"));
    expected.add(new Orbit("K)L"));
    //when
    List<Orbit> actual = this.mapToOrbits(input);
    //then
    Assert.assertEquals(expected, actual);
  }
}
