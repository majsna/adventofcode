package main.programs;

import java.util.List;
import java.util.Random;
import main.utils.ListToStringMapper;
import main.utils.StringToListMapper;

public class DayTwoPartTwo {

  public static final String EXPECTED_OUTPUT = "19690720";
  DayTwo dayTwo = new DayTwo();
  Random generator = new Random();
  int noun;
  int verb;

  public String calculateNounAndVerb(String input) {
    String output = null;
    while (!EXPECTED_OUTPUT.equals(output)) {
      String newInput = replace1and2withRandomNounAndVerb(input);
      output = dayTwo.calculateIntcode(newInput);
      System.out.println(output);
    }

    System.out.println("And the winners are: ");

    int result = 100 * noun + verb;
    return result + "";
  }

  private String replace1and2withRandomNounAndVerb(String input) {
    List<Integer> memory = StringToListMapper.map(input, ",");
    noun = generator.nextInt(100);
    verb = generator.nextInt(100);
    memory.set(1, noun);
    memory.set(2, verb);
    return ListToStringMapper.map(memory);
  }

}
