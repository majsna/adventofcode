package main.programs;

import main.utils.ListToStringMapper;
import main.utils.StringToListMapper;

public class DayFive extends DayTwo {

  public static final int INPUT_OPTCODE = 3;
  public static final int OUTPUT_OPTCODE = 4;

  public static final int POSITION_MODE = 0;
  public static final int IMMEDIATE_MODE = 1;

  @Override
  public String calculateIntcode(String input) {
    memory = StringToListMapper.map(input, ",");
    try {
      int couterMax = 0;
      int counter = 0;
      for (Integer i : memory) {
        if (counter == couterMax) {
          counter = 0;
        }
        switch (counter) {
          case 0:
            instruction = i;
            counter = setCounterMaxBasedOnInstrucition(i);
            break;
          case 1:
            firstParamPosition = i;
            break;
          case 2:
            secondParamPosition = i;
            break;
          case 3:
            resultPosition = i;
//            calculateResultAndMoveToPosition();
            break;
          default:
            throw new IllegalArgumentException("Wrong counter value.");
        }
        counter++;
      }
    } catch (RuntimeException e) {
    }
    resultMemory = ListToStringMapper.map(memory);
    return memory.get(0).toString();
  }

  private int setCounterMaxBasedOnInstrucition(Integer i) {
    switch (i) {
      case ADD_NUMBERS_OPTCODE:
      case MULTIPLY_NUMBERS_OPTCODE:
        return 4;
      case INPUT_OPTCODE:
        return 2;
      case OUTPUT_OPTCODE:
        return 3;
      default:
        throw new IllegalArgumentException("Wrong opcode.");
    }
  }

}
