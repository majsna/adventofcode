package main.programs;

import java.util.List;

public class DayOne {

  public int calculateFuel(List<Integer> givenMassList) {
    int result = 0;
    for (Integer mass : givenMassList) {
      result += calculateFuelForOneDay(mass);
    }
    return result;
  }

  private int calculateFuelForOneDay(int i) {
    int result = 0;
    int leftOver = i / 3 - 2;
    while (leftOver > 0) {
      result += leftOver;
      leftOver = leftOver / 3 - 2;
    }
    return result;
  }
}