package main.programs.daythree;

import java.util.Objects;

public class Element {

  public Direction direction;
  public int value;

  public Element(Direction direction, int value) {
    this.direction = direction;
    this.value = value;
  }

  public Element(String elStr) {
    switch (elStr.charAt(0)) {
      case 'R':
        this.direction = Direction.R;
        break;
      case 'L':
        this.direction = Direction.L;
        break;
      case 'U':
        this.direction = Direction.U;
        break;
      case 'D':
        this.direction = Direction.D;
        break;
    }
    this.value = Integer.valueOf(elStr.substring(1));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Element element = (Element) o;
    return value == element.value &&
        direction == element.direction;
  }

  @Override
  public int hashCode() {
    return Objects.hash(direction, value);
  }

  @Override
  public String toString() {
    return "Element{" +
        "direction=" + direction +
        ", value=" + value +
        '}';
  }
}
