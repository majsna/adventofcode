package main.programs.daythree;

import java.util.List;

public class DayThreePartTwo extends DayThree {

  @Override
  public String calculate(String input) {
    parseInput(input);
    List<Section> sectionsInTheFirstWire = getSectionsFrom(firstWire);
    List<Section> sectionsInTheSecondWire = getSectionsFrom(secondWire);
    double result = findFewestStepsCombinationForIntersaction(sectionsInTheFirstWire,
        sectionsInTheSecondWire);
    return result + "";
  }

  private double findFewestStepsCombinationForIntersaction(List<Section> firstWireSections,
      List<Section> secontWireSections) {
    double minDistance = Double.MAX_VALUE;
    double firsWireDistance = 0;
    double secondWireDistance = 0;
    for (int i = 0; i < firstWireSections.size(); i++) {
      Section one = firstWireSections.get(i);
      firsWireDistance += one.length();
      for (int j = 0; j < secontWireSections.size(); j++) {
        Section two = secontWireSections.get(j);
        if (j == 0) {
          secondWireDistance = two.length();
        } else {
          secondWireDistance += two.length();
        }
        Point commonPoint = CommonPointCalc
            .findCommonPoint(one, two);
        if (commonPoint != null) {
          double distance =
              firsWireDistance + secondWireDistance - two.distanceFromB(commonPoint) - one
                  .distanceFromB(commonPoint);
          minDistance = distance < minDistance ? distance : minDistance;
        }

      }
    }
    return minDistance;
  }

}
