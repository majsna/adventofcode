package main.programs.daythree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import main.programs.Calculator;

public class DayThree implements Calculator {

  public List<Element> firstWire;
  public List<Element> secondWire;

  public DayThree() {
  }

  @Override
  public String calculate(String input) {
    parseInput(input);
    List<Section> sectionsInTheFirstWire = getSectionsFrom(firstWire);
    List<Section> sectionsInTheSecondWire = getSectionsFrom(secondWire);
    int result = findClosesIntersectionDistance(sectionsInTheFirstWire, sectionsInTheSecondWire);
    return result + "";
  }

  public int findClosesIntersectionDistance(List<Section> firstWireSections,
      List<Section> secontWireSections) {
    List<Point> commonPoints = new ArrayList<>();
    findCommonPoints(firstWireSections, secontWireSections, commonPoints);
    List<Double> intersectionDistances = commonPoints.stream().filter(point -> point != null)
        .map(point -> Math.abs(point.x) + Math.abs(point.y))
        .collect(Collectors.toList());
    return Collections.min(intersectionDistances).intValue();
  }

  protected void findCommonPoints(List<Section> firstWireSections, List<Section> secontWireSections,
      List<Point> commonPoints) {
    firstWireSections.stream()
        .forEach(sectionOne -> {
          secontWireSections.stream()
              .forEach(sectionTwo -> {
                Point commonPoint = CommonPointCalc.findCommonPoint(sectionOne,sectionTwo);
                if (commonPoint != null) {
                  commonPoints.add(commonPoint);
                }
              });
        });
  }

  public void parseInput(String input) {
    String[] wires = input.split("S");
    firstWire = Arrays.asList(wires[0].split(","))
        .stream()
        .map(elStr -> new Element(elStr))
        .collect(Collectors.toList());
    secondWire = Arrays.asList(wires[1].split(","))
        .stream()
        .map(elStr -> new Element(elStr))
        .collect(Collectors.toList());
  }

  public List<Section> getSectionsFrom(List<Element> firstWire) {
    List<Section> sections = new ArrayList<>();
    for (int i = 0; i < firstWire.size(); i++) {
      Element el = firstWire.get(i);
      Section section = new Section();
      if (i == 0) {
        Point a = new Point(0, 0);
        section.setA(a);
        section.setB(calculatePointB(a, el));
      } else {
        Point a = sections.get(i - 1).getB();
        section.setA(a);
        section.setB(calculatePointB(a, el));
      }
      sections.add(section);
    }
    return sections;
  }

  public Point calculatePointB(Point a, Element elAfter) {
    Point b = new Point();
    switch (elAfter.direction) {
      case R:
        b.x = a.x + elAfter.value;
        b.y = a.y;
        break;
      case L:
        b.x = a.x - elAfter.value;
        b.y = a.y;
        break;
      case U:
        b.x = a.x;
        b.y = a.y + elAfter.value;
        break;
      case D:
        b.x = a.x;
        b.y = a.y - elAfter.value;
        break;
      default:
        throw new IllegalArgumentException("Wrong Direction.");
    }
    return b;
  }


}
