package main.programs.daythree;

public enum Direction {

  R("Right"),
  L("Left"),
  D("Down"),
  U("Up");

  private final String description;

  Direction(String description){
    this.description = description;
  }

}
