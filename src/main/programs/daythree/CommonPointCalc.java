package main.programs.daythree;

public class CommonPointCalc {

  public static Point findCommonPoint(Section one, Section two) {
    Point A = one.a;
    Point B = one.b;
    Point C = two.a;
    Point D = two.b;

    // Line AB represented as a1x + b1y = c1
    double a1 = B.y - A.y;
    double b1 = A.x - B.x;
    double c1 = a1 * (A.x) + b1 * (A.y);

    // Line CD represented as a2x + b2y = c2
    double a2 = D.y - C.y;
    double b2 = C.x - D.x;
    double c2 = a2 * (C.x) + b2 * (C.y);

    double determinant = a1 * b2 - a2 * b1;

    if (determinant == 0) {
      // The lines are parallel.
      return null;
    } else {
      Point commonPoint = new Point();
      commonPoint.x = (b2 * c1 - b1 * c2) / determinant;
      commonPoint.y = (a1 * c2 - a2 * c1) / determinant;
      if (isCommonPointPartOfEachSection(A, B, C, D, commonPoint) &&
          commonPointIsNotStartPoint(commonPoint)) {
        return commonPoint;
      } else {
        return null;
      }
    }
  }

  private static boolean isCommonPointPartOfEachSection(Point a, Point b, Point c, Point d,
      Point commonPoint) {
    return commonPoint.x >= Math.min(a.x, b.x) &&
        commonPoint.x <= Math.max(a.x, b.x) &&
        commonPoint.x >= Math.min(c.x, d.x) &&
        commonPoint.x <= Math.max(c.x, d.x) &&
        commonPoint.y >= Math.min(a.y, b.y) &&
        commonPoint.y <= Math.max(a.y, b.y) &&
        commonPoint.y >= Math.min(c.y, d.y) &&
        commonPoint.y <= Math.max(c.y, d.y);
  }

  private static boolean commonPointIsNotStartPoint(Point commonPoint) {
    return (commonPoint.x != 0 || commonPoint.x != -0) && (commonPoint.y != 0
        || commonPoint.y != -0);
  }

}
