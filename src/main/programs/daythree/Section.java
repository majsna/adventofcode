package main.programs.daythree;

import java.util.Objects;

public class Section {

  public Point a;
  public Point b;

  public Section(Point a, Point b) {
    this.a = a;
    this.b = b;
  }

  public Section() {
  }

  public Point getA() {
    return a;
  }

  public void setA(Point a) {
    this.a = a;
  }

  public Point getB() {
    return b;
  }

  public void setB(Point b) {
    this.b = b;
  }

  public double length(){
    return Math.sqrt((Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)));
  }

  public double distanceFromB(Point C){
    return Math.sqrt((Math.pow(C.x - b.x, 2) + Math.pow(C.y - b.y, 2)));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Section section = (Section) o;
    return Objects.equals(a, section.a) &&
        Objects.equals(b, section.b);
  }

  @Override
  public int hashCode() {
    return Objects.hash(a, b);
  }
}
