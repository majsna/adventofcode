package main;

import java.util.Scanner;
import main.fascade.ProgramsFascade;
import main.utils.ScannerInputValidator;

public class UserInterface {

  private final String EXIT_CODE = "exit";
  private ProgramsFascade programsFascade = new ProgramsFascade();
  private Scanner scanner = new Scanner(System.in);
  private String input = "";
  private String output = "";
  private int programNo;

  protected void run() {
    while (true) {
      printChooseProgramMessageAndScanNextLine();
      while (!isInputValid()) {
        printInvalidInputMessage();
        printChooseProgramMessageAndScanNextLine();
      }
      programNo = Integer.parseInt(input);
      printEnterInputMessage();
      scanInput();
      calculateAndPrintOutput(programNo, input);
    }
  }

  private void scanInput() {
    if (programNo == 8) {
      scanMultipleLines();
    } else {
      scanNextLine();
    }
  }

  private void scanMultipleLines() {
    String line = "";
    input = "";
    while(!line.equals("STOP")){
      input = input + line + "\n";
      line = scanner.nextLine();
    }
    input = input.trim();
  }

  private boolean isInputValid() {
    try {
      int i = Integer.parseInt(input);
      return i > 0 && i < 9;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  private void calculateAndPrintOutput(int programNo, String input) {
    output = programsFascade.chooseProgramAndCalculate(programNo, input);
    System.out.println("Thank you. The output of the program is:\n"
        + output + "\n"
        + "************************************\n");
  }

  private void scanNextLine() {
    input = scanner.nextLine();
    if (EXIT_CODE.equals(input)) {
      System.exit(0);
    }
  }

  private void printInvalidInputMessage() {
    System.out.println("Invalid program number. Try again.");
  }

  private void printEnterInputMessage() {
    System.out.println("Please enter the input:");
  }

  private void printChooseProgramMessageAndScanNextLine() {
    System.out.println("Please enter the program code you want to use. Options are:\n"
        + "1 - DayOne\n"
        + "2 - DayTwo\n"
        + "3 - DayTwoPartTwo\n"
        + "4 - DayThree\n"
        + "5 - DayThreePartTwo\n"
        + "6 - DayFour(input does not matter)\n"
        + "7 - DayFourPartTwo(input does not matter)\n"
        + "8 - DaySix\n"
        + "exit - if you want to... exit ;)"
    );
    scanNextLine();
  }
}
