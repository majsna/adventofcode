package main.utils;

public class ScannerInputValidator {

  public static boolean isValid(String input){
    try {
      int i = Integer.parseInt( input );
      return i > 0 && i < 7;
    }
    catch( NumberFormatException e ) {
      return false;
    }
  }

}
