package main.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringToListMapper {

  public static List<Integer> map(String input, String splitter) {
    return Arrays.asList(input.split(splitter))
        .stream()
        .map(element -> Integer.parseInt(element))
        .collect(Collectors.toList());
  }

}
