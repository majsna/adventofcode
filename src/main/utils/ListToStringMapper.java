package main.utils;

import java.util.List;

public class ListToStringMapper {

  public static String map(List<Integer> list){
    StringBuilder stringBuilder = new StringBuilder();
    list.stream().forEach(integer -> stringBuilder.append(integer).append(","));
    return  stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1 );
  }

}
