package main.fascade;

import main.programs.DayFour;
import main.programs.DayFourPartTwo;
import main.programs.DaySix;
import main.programs.DayTwo;
import main.programs.DayTwoPartTwo;
import main.programs.DayOne;
import main.programs.daythree.DayThree;
import main.programs.daythree.DayThreePartTwo;

public class ProgramsFascade {

  DayOne fuelCalculator;
  DayTwo dayTwo;
  DayTwoPartTwo dayTwoPartTwo;
  DayThree dayThree;
  DayThreePartTwo dayThreePartTwo;
  DayFour dayFour;
  DayFourPartTwo dayFourPartTwo;
  DaySix daySix;

  public String chooseProgramAndCalculate(int programNo, String input) {
    String output = null;
    switch (programNo) {
      case 1:
        fuelCalculator = new DayOne();
        output = "dupa";
        break;
      case 2:
        dayTwo = new DayTwo();
        output = dayTwo.calculateIntcode(input);
        break;
      case 3:
        dayTwoPartTwo = new DayTwoPartTwo();
        output = dayTwoPartTwo.calculateNounAndVerb(input);
        break;
      case 4:
        dayThree = new DayThree();
        output = dayThree.calculate(input);
        break;
      case 5:
        dayThreePartTwo = new DayThreePartTwo();
        output = dayThreePartTwo.calculate(input);
        break;
      case 6:
        dayFour = new DayFour();
        output = dayFour.calculate(input);
        break;
      case 7:
        dayFourPartTwo = new DayFourPartTwo();
        output = dayFourPartTwo.calculate(input);
        break;
        case 8:
        daySix = new DaySix();
        output = daySix.calculate(input);
        break;
    }
    return output;
  }

}
